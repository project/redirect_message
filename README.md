## CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Maintainers

## INTRODUCTION

Implements a redirect message based on each redirect that is created.

## REQUIREMENTS

This module requires redirect module.

## INSTALLATION

Install as normal (see
http://drupal.org/documentation/install/modules-themes).

## CONFIGURATION

This module doesn’t have any configuration.

## MAINTAINERS

Neslee Canil Pinto: https://www.drupal.org/u/neslee-canil-pinto
Stephan Zeidler: https://www.drupal.org/u/szeidler
