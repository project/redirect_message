<?php

namespace Drupal\Tests\redirect_message\Functional;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the redirect message functionality.
 *
 * @group redirect_message
 */
class RedirectMessageFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'text',
    'options',
    'redirect',
    'redirect_message',
  ];

  /**
   * A user with permissions to work with Redirects.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $redirectUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Log in as a content author who can create articles and use the revive
    // field.
    $this->redirectUser = $this->drupalCreateUser([
      'administer redirects',
    ]);
    $this->drupalLogin($this->redirectUser);
  }

  /**
   * Test that saving a redirect with a message will show.
   *
   * @dataProvider providerStatusMessageTypes
   */
  public function testRedirectMessageIsShown($type) {
    // Add the redirect.
    $this->drupalGet('admin/config/search/redirect/add');
    $sourceUrl = 'redirect-with-' . $type . '-message';
    $message = 'Check out my ' . $type . ' redirect message!';
    $edit = [
      'redirect_source[0][path]' => $sourceUrl,
      'redirect_redirect[0][uri]' => '<front>',
      'message[0][value]' => $message,
      'message_type' => $type,
    ];
    $this->submitForm($edit, 'Save');

    // Open the redirect and verify the message appears.
    $this->drupalGet($sourceUrl);
    $this->assertSession()->statusMessageContains($message, $type);
  }

  /**
   * Test that not setting a redirect message does not trigger a message.
   */
  public function testNoRedirectMessage() {
    // Add the redirect.
    $this->drupalGet('admin/config/search/redirect/add');
    $sourceUrl = 'redirect-no-message';
    $edit = [
      'redirect_source[0][path]' => $sourceUrl,
      'redirect_redirect[0][uri]' => '<front>',
    ];
    $this->submitForm($edit, 'Save');

    // Open the redirect and verify the message appears.
    $this->drupalGet($sourceUrl);
    $this->assertSession()->statusMessageNotExists();
  }

  /**
   * Provides the different status message types.
   *
   * @return array
   *   Status message types.
   */
  public static function providerStatusMessageTypes(): array {
    return [
      [MessengerInterface::TYPE_STATUS],
      [MessengerInterface::TYPE_WARNING],
      [MessengerInterface::TYPE_ERROR],
    ];
  }

}
