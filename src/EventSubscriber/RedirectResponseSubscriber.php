<?php

namespace Drupal\redirect_message\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\redirect\RedirectRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Redirect message subscriber for controller responses.
 */
class RedirectResponseSubscriber implements EventSubscriberInterface {

  /**
   * The redirect repository.
   *
   * @var \Drupal\redirect\RedirectRepository
   */
  protected $redirectRepository;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a RedirectRequestSubscriber object.
   *
   * @param \Drupal\redirect\RedirectRepository $redirect_repository
   *   The redirect entity repository.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(RedirectRepository $redirect_repository, MessengerInterface $messenger) {
    $this->redirectRepository = $redirect_repository;
    $this->messenger = $messenger;
  }

  /**
   * Handles the redirect message if any redirect is used found.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onKernelResponseCheckRedirect(ResponseEvent $event) {
    $response = $event->getResponse();
    if ($response instanceof RedirectResponse) {
      // Check if we have a redirect id header.
      if ($response->headers->has('X-Redirect-ID')) {
        $redirectId = $response->headers->get('X-Redirect-ID');
        if ($redirect = $this->redirectRepository->load($redirectId)) {
          if (!$redirect->get('message_type')->isEmpty() && !$redirect->get('message')->isEmpty()) {
            $messageType = $redirect->get('message_type')->value;
            $messageValue = $redirect->get('message')->value;
            $messageFormat = $redirect->get('message')->format;
            $message = check_markup($messageValue, $messageFormat);
            switch ($messageType) {
              case 'error':
                $this->messenger->addError($message);
                break;

              case 'status':
                $this->messenger->addStatus($message);
                break;

              case 'warning':
                $this->messenger->addWarning($message);
                break;
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onKernelResponseCheckRedirect', 33];
    return $events;
  }

}
